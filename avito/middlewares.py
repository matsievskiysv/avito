# Define here the models for your spider middleware
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/spider-middleware.html

from scrapy import signals

import requests
from stem import Signal
from stem.control import Controller
import random


# useful for handling different item types with a single interface
from itemadapter import is_item, ItemAdapter


# class AvitoSpiderMiddleware:
#     # Not all methods need to be defined. If a method is not defined,
#     # scrapy acts as if the spider middleware does not modify the
#     # passed objects.

#     @classmethod
#     def from_crawler(cls, crawler):
#         # This method is used by Scrapy to create your spiders.
#         s = cls()
#         crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
#         return s

#     def process_spider_input(self, response, spider):
#         # Called for each response that goes through the spider
#         # middleware and into the spider.

#         # Should return None or raise an exception.
#         return None

#     def process_spider_output(self, response, result, spider):
#         # Called with the results returned from the Spider, after
#         # it has processed the response.

#         # Must return an iterable of Request, or item objects.
#         for i in result:
#             yield i

#     def process_spider_exception(self, response, exception, spider):
#         # Called when a spider or process_spider_input() method
#         # (from other spider middleware) raises an exception.

#         # Should return either None or an iterable of Request or item objects.
#         pass

#     def process_start_requests(self, start_requests, spider):
#         # Called with the start requests of the spider, and works
#         # similarly to the process_spider_output() method, except
#         # that it doesn’t have a response associated.

#         # Must return only requests (not items).
#         for r in start_requests:
#             yield r

#     def spider_opened(self, spider):
#         spider.logger.info('Spider opened: %s' % spider.name)

class AvitoCheckMiddleware:
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the spider middleware does not modify the
    # passed objects.

    def process_request(self, request, spider):
        spider.logger.info('Request: %s', request)
        spider.logger.info('Request headers: %s', request.headers)
        spider.logger.info('Request meta: %s', request.meta)
        return None


def change_ip(logger):
    with Controller.from_port(port=9051) as c:
        c.authenticate()
        c.signal(Signal.NEWNYM)

    ip = requests.get("https://ident.me",
                      proxies={
                          "http": "http://127.0.0.1:8118",
                          "https": "http://127.0.0.1:8118"
                      }).text
    logger.info(f"New IP: {ip}")

with open("./avito/user_agents.txt", "r") as f:
    user_agents = [x.strip() for x in f.readlines()]


class AvitoDownloaderMiddleware:
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the downloader middleware does not modify the
    # passed objects.
    download_counter = 0
    DOWNLOAD_PER_IP = 20

    def process_request(self, request, spider):
        # Called for each request that goes through the downloader
        # middleware.

        # Must either:
        # - return None: continue processing this request
        # - or return a Response object
        # - or return a Request object
        # - or raise IgnoreRequest: process_exception() methods of
        #   installed downloader middleware will be called
        # request.meta['proxy'] = 'http://167.172.109.12:44465'
        if self.download_counter >= self.DOWNLOAD_PER_IP or self.download_counter == 0:
            global user_agents
            # change_ip(spider.logger)
            self.download_counter = 1
            spider.user_agent = random.choice(user_agents)
            spider.logger.info(f"User agent: {spider.user_agent}")
        self.download_counter += 1
        return None

