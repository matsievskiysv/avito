import scrapy

from ..items import AvitoLoader, AvitoItem

class MainSpider(scrapy.Spider):
    name = 'main'
    allowed_domains = ['avito.ru']
    metro = '-'.join([str(n) for n in [22, 39, 68, 69, 70, 106, 136, 146]])
    minpage = 1
    maxpage = 1
    minprice = 500
    maxprice = 1500
    location = 'moskva'
    search_name = '+'.join(['коляска', 'трость'])  # search name
    base_url = r'https://www.avito.ru/{}?metro={}&q={}&p={}'
    start_urls = [base_url.format(location, metro, search_name, minpage)]
    db = "data.sqlite"

    def create(self, cursor):
        cursor.execute('drop table data')
        cursor.execute('''create table data(
link text not null,
name text not null,
price integer not null,
date text not null,
images text not null,
seller text not null,
params text not null,
description text not null,
location text not null)''')

    def add(self, cursor, item):
        cursor.execute('insert into data values (?,?,?,?,?,?,?,?,?)',
                       (item['link'], item['name'], item['price'], item['date'], ';'.join(item['images']),
                        item['seller'], item['params'], item['description'], item['location']))

    def parse(self, response):
        # get page range
        pages = list(map(int, filter(str.isdigit,
                                          response.css('span[data-marker^="page("]::text').getall())))
        for p in range(max(self.minpage,min(pages)), min(max(pages),self.maxpage)+1):
            yield response.follow(self.base_url.format(self.location, self.metro, self.search_name, p),
                                  callback=self.items)


    def items(self, response):
        yield from response.follow_all(css='div[class^="iva-item-root"] div[class^=iva-item-title] a',
                                       callback=self.item)


    def item(self, response):
        loader = AvitoLoader(item=AvitoItem(),
                             response=response)
        loader.add_value('link', response.url)
        loader.add_css('name', '.item-view-content h1.title-info-title *::text')
        loader.add_css('price', '.item-view-content [itemprop="price"]::text')
        loader.add_css('date', 'div.title-info-metadata-item-redesign::text')
        loader.add_css('images', '.gallery-imgs-container .gallery-img-frame[data-url]::attr(data-url)')
        loader.add_css('seller', '.seller-info-name *::text')
        loader.add_css('params', '.item-params *::text')
        loader.add_css('description', '.item-description *::text')
        loader.add_css('location', '[itemprop="address"] *::text')
        yield loader.load_item()
