# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
from scrapy.exceptions import DropItem
from scrapy.exporters import BaseItemExporter

import sqlite3

class PriceFilterPipeline:
    def process_item(self, item, spider):
        if spider.minprice <= item['price'] <= spider.maxprice:
            return item
        else:
            raise DropItem


class SQLiteExporter(BaseItemExporter):
    def __init__(self, file, create, add, **kwargs):
        super().__init__(dont_fail=False, **kwargs)
        self.file = file
        self.create = create
        self.add = add

    def start_exporting(self):
        self.conn = sqlite3.connect(self.file)
        self.cursor = self.conn.cursor()
        self.create(self.cursor)

    def finish_exporting(self):
        self.conn.commit()
        self.conn.close()

    def export_item(self, item):
        self.add(self.cursor, item)

class DBExportPipeline:
    def open_spider(self, spider):
        self.exporter = SQLiteExporter(spider.db, spider.create, spider.add)
        self.exporter.start_exporting()

    def close_spider(self, spider):
        self.exporter.finish_exporting()

    def process_item(self, item, spider):
        self.exporter.export_item(item)
        return item
