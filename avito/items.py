# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import re
import datetime

import scrapy
import scrapy.loader as loader
import itemloaders.processors as processors

class AvitoItem(scrapy.Item):
    # define the fields for your item here like:
    link = scrapy.Field()
    name = scrapy.Field()
    price = scrapy.Field()
    date = scrapy.Field()
    images = scrapy.Field()
    seller = scrapy.Field()
    params = scrapy.Field()
    description = scrapy.Field()
    location = scrapy.Field()

MONTHS = {'января': 1,
          'февраля': 2,
          'марта': 3,
          'апреля': 4,
          'мая': 5,
          'июня': 6,
          'июля': 7,
          'августа': 8,
          'сентября': 9,
          'октября': 10,
          'ноября': 11,
          'декабря': 12}
DELTA = {'сегодня': 0,
         'вчера': 1,}

fulldate = re.compile(r'(?i)(?P<day>\d{1,2})\s+(?P<month>\w+)\s+в\s+(?P<hour>\d{1,2}):(?P<minute>\d{1,2})')
deltadate = re.compile(r'(?i)(?P<deltaday>\w+)\s+в\s+(?P<hour>\d{1,2}):(?P<minute>\d{1,2})')

def normalize_date(s):
    now = datetime.datetime.now()
    match = re.match(fulldate, s)
    if match is not None:
        return datetime.datetime(now.year,
                                 MONTHS.get(match.group('month'), 1),
                                 int(match.group('day')),
                                 int(match.group('hour')),
                                 int(match.group('minute'))).isoformat()
    else:
        match = re.match(deltadate, s)
        if match is None:
            return now.isoformat()
        else:
            return (datetime.datetime(now.year,
                                      now.month,
                                      now.day,
                                      int(match.group('hour')),
                                      int(match.group('minute'))) -
                    datetime.timedelta(days=DELTA.get(match.group('deltaday'), 0))).isoformat()

class AvitoLoader(loader.ItemLoader):
    default_input_processor = processors.MapCompose(str.strip, lambda s: s if len(s) > 0 else None)
    default_output_processor = processors.Join(' ')

    images_out = processors.Identity()
    params_in = processors.MapCompose(str.strip,
                                      lambda s: s if len(s) > 0 else None,
                                      lambda s: s if s != 'Состояние:' else None)
    price_in = processors.MapCompose(str.strip,
                                     lambda s: s.replace(' ', ''),
                                     int)
    price_out = processors.TakeFirst()
    date_in = processors.MapCompose(str.strip,
                                    normalize_date)
